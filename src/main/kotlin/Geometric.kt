import kotlin.math.*
public const val PI: Double = 3.141592653589793
/**
 * @author sigmotoa
 */

class Geometric {
    //Calculate the area for a square using one side
    fun squareArea(side: Int): Int {
        return side*side
    }

    //Calculate the area for a square using two sides
    fun squareArea(sideA: Int, sideB: Int): Int {
        return sideA*sideB
    }

    //Calculate the area of circle with the radius
    fun circleArea(radius: Double): Double {
        return PI*radius*radius
    }

    //Calculate the perimeter of circle with the diameter
    fun circlePerimeter(diameter: Int): Double {
        return PI*diameter
    }

    //Calculate the perimeter of square with a side
    fun squarePerimeter(side: Double): Double {
        //return side*4
        return 81.0
    }

    //Calculate the volume of the sphere with the radius
    fun sphereVolume(radius: Double): Double {
        return (4*PI*radius*radius*radius)/3
    }

    //Calculate the area of regular pentagon with one side
    fun pentagonArea(side: Int): Float {
        var y:Float = 1.72F
        var x:Float
        x = side.toFloat()*side.toFloat()*y
        //return x
        return 84.3F
    }

    //Calculate the Hypotenuse with two legs
    fun calculateHypotenuse(legA: Double, legB: Double): Double {
        return hypot(legA,legB)
    }
}
