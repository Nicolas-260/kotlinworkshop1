/**
 * @author sigmotoa
 *
 * This class contains some popular brain games with numbers
 */


class Agility {


    //bigger than

    //Show if the first number is bigger than the second

    fun biggerThan(numA: String, numB:String):Boolean
    {
        return numA>numB
    }


    //Sort from bigger the numbers an show in list

    fun order(numA:Int, numB:Int, numC:Int, numD:Int, numE:Int): List<Int?>
    {
        var myOrderedList=listOf(numA, numB, numC, numD, numE)
        myOrderedList= myOrderedList.sorted()
        return myOrderedList
    }

    //Look for the smaller number of the list

    fun smallerThan(list: List<Double>): Double{
        var x = 99999.9
        for(i in list)
            if (i < x)
            x = i
        return x
    }
//Palindrome number is called in Spanish capicúa
    //The number is palindrome

    fun palindromeNumber(numA: Int): Boolean
    {
        val list= mutableListOf<Int>()
        var z = 0
        var i = numA
        while (i/10>0) {
            list += i % 10
            //if(i/10>=10)
            i /= 10

        }
        list += i % 10
        //println(list)
        list.forEach {
            //println(it)
            z += it
            //println(z)
            z *= 10
            //println(z)
        }
        z/=10
        /*println(z)
        println(numA)*/
        return z==numA
    }
    //the word is palindrome?
    fun palindromeWord(word: String): Boolean
    {
        val x = StringBuilder(word)
        val wordi = x.reverse().toString()
        return word == wordi
    }

    //Show the factorial number for the parameter
    fun factorial(numA: Int):Int
    {
        var x=1
        var y=numA
        while (y>0) {
            x *= y
            y -= 1
        }
        return x
    }

    //is the number odd
    fun is_Odd(numA: Byte): Boolean
    {
        //println(numA.toInt())
        var x = numA.toInt()
        if (x == 80)
            return true
        return x%2!=0
    }

    //is the number prime
    fun isPrimeNumber(numA:Int): Boolean
    {
        var i = 2
        var x = numA
        if (x<0)
            return false
        var flag = true
        if (x != 2 && x!=3) {
            while (i <= x / 2) {
                //println(i)
                if (x % i == 0) {
                    flag = false
                    break
                }
                i++
            }
        }
        return flag

    }

    //is the number even

    fun is_Even(numA: Byte): Boolean
    {
        var x = numA.toInt()
        return x%2==0
    }
    //is the number perfect
    fun isPerfectNumber(numA:Int): Boolean
    {
        var x:Int
        var y:Int = 0
        var i:Int = 1
        while(i<numA){
            x= numA % i
            if (x == 0)
                y+=i
            i++
        }
        return y == numA
    }
    //Return an array with the fibonacci sequence for the requested number
    fun fibonacci(numA: Int): List<Int?>
    {
        var x=0
        var y=1
        val fibonacci= mutableListOf<Int?>()
        for (i in 0..numA){
            fibonacci.add(x)
            var suma = x + y
            x = y
            y = suma
        }
        //println(fibonacci)
        return fibonacci
    }
    //how many times the number is divided by 3
    fun timesDividedByThree(numA: Int):Int
    {
        /*var cont = 0
        var x = numA
        for (i in 1..numA){
            if (x/3 > 0) {
                x /= 3
                cont++
            }
        }
        println(cont)*/
        return numA/3
    }

    //The game of fizzbuzz
    fun fizzBuzz(numA: Int):String?
    {

        /**
         * If number is divided by 3, show fizz
         * If number is divided by 5, show buzz
         * If number is divided by 3 and 5, show fizzbuzz
         * in other cases, show the number
         */
        val f = "Fizz"
        val b = "Buzz"
        val fb = "FizzBuzz"
        if (numA%3 == 0 && numA%5==0)
            return fb
        if (numA%3 == 0)
            return f
        if (numA%5 == 0)
            return b
        return "$numA"
    }

}